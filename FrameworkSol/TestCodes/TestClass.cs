﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RelevantCodes.ExtentReports;

namespace TestCodes
{
    [TestFixture]
    public class TestClass
    {
        [Test]
        public void ReportTesting()
        {
            ExtentReports report = new ExtentReports(@"G:\Reports\SampleReport.html");
            ExtentTest test = report.StartTest("Automation Test");

            test.Log(LogStatus.Info, "Starting the Test now");
            //selenium codes
            test.Log(LogStatus.Pass, "Test has been passed");

            report.EndTest(test);
            report.Flush();
        }

    }
}
