﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DynamicNames
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 0;
            while (i <= 3)
            {
                string fileName = DateTime.Now.ToString().Replace(" ", "").Replace("/", "").Replace(":", "");
                Console.WriteLine(fileName);
                Thread.Sleep(2000);
                i++;
            }
        }
    }
}
