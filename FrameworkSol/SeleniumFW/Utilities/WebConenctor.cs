﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using NUnit.Framework;
using System.Configuration;
using System.Collections.ObjectModel;
using System.Drawing;
using RelevantCodes.ExtentReports;

namespace SeleniumFW.Utilities
{
    public class WebConenctor
    {
        public IWebDriver driver = null;
        public string customFileName = DateTime.Now.ToString().Replace(" ", "").Replace("/", "").Replace(":", "");
        public ExtentReports report = ExtentManager.getInstance();
        public ExtentTest test;

        public void OpenBrowser(string browserName)
        {
            test.Log(LogStatus.Info, "Selected browser is : " + browserName);
            string browser = browserName.ToLower();
            try
            {
                if (browser.Equals("chrome"))
                {
                    driver = new ChromeDriver(FileLocations.DriverLocation);
                }
                else if (browser.Equals("firefox") || browser.Equals("mozilla"))
                {
                    driver = new FirefoxDriver();
                }
                else if (browser.Equals("internetexplorer") || browser.Equals("ie"))
                {
                    driver = new InternetExplorerDriver(FileLocations.DriverLocation);
                }
                else
                {
                    test.Log(LogStatus.Info, "Invalid browser selected!!!");
                    Assert.Fail("Not a valid Browser");
                }
                driver.Manage().Window.Maximize();
                test.Log(LogStatus.Info, "Browser has been maximized");
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                test.Log(LogStatus.Info, "Implicit wait of 10 seconds has been configured successfully");
            }
            catch (Exception)
            {
                test.Log(LogStatus.Fail, "Browser was not opened successfully");
                ReportFail("Browser was not opened successfully");
            }
        }

        public void NavigateTo(string locator)
        {
            test.Log(LogStatus.Info, "Trying to navigating to URL : " + ConfigurationManager.AppSettings[locator]);
            driver.Url = ConfigurationManager.AppSettings[locator];
        }

        public IWebElement GetElement(string locator)
        {
            IWebElement ele = null;
            try
            {
                ele = driver.FindElement(By.XPath(ConfigurationManager.AppSettings[locator]));
                test.Log(LogStatus.Info, "Element : " + locator + " has been found");
            }
            catch (Exception)
            {
                test.Log(LogStatus.Info, "Element : " + locator + " could not be found");
                ReportFail("Element " + locator + " could not be found");
            }
            return ele;
        }

        public void Type(string locator, string data)
        {
            test.Log(LogStatus.Info, "Trying to type : " + data + " in " + locator);
            GetElement(locator).SendKeys(data);
        }

        public void ClickOnElement(string locator)
        {
            test.Log(LogStatus.Info, "Clicking the " + locator);
            GetElement(locator).Click();
        }

        public void TakeMyScreenShots()
        {  
            Screenshot src = ((ITakesScreenshot)driver).GetScreenshot();
            string absoluteFileName = FileLocations.ScreenShotLocation + "\\" + customFileName + ".png";
            src.SaveAsFile(absoluteFileName, ScreenshotImageFormat.Png);
            test.Log(LogStatus.Info, "Screenshot has been captured and saved under the location : "  + absoluteFileName);
            test.Log(LogStatus.Info, "Please find the screen shot here : " + test.AddScreenCapture(absoluteFileName));
        }

        public bool IsElementPresent(string locator)
        {  
             ReadOnlyCollection<IWebElement> allEle = driver.FindElements(By.XPath(ConfigurationManager.AppSettings[locator]));
            if (allEle.Count != 0)
            {
                test.Log(LogStatus.Info, "Element : " + locator + " has been found");
                return true;
            }
            else
            {
                test.Log(LogStatus.Info, "Element : " + locator + " could not be found");
                return false;
            }
            //return (allEle.Count != 0) ? true : false;
        }

        public void ReportPass(string msg)
        {
            test.Log(LogStatus.Pass, msg);
            TakeMyScreenShots();
        }

        public void ReportIgnore(string msg)
        {
            test.Log(LogStatus.Skip, msg);
            TakeMyScreenShots();
            Assert.Ignore(msg);
        }

        public void ReportFail(string msg)
        {
            test.Log(LogStatus.Fail, msg);
            TakeMyScreenShots();
            Assert.Fail(msg);
        }

        public void CloseBrowser()
        {
            if (driver != null)
                driver.Quit();
            if (report != null)
            {
                report.EndTest(test);
                report.Flush();
            }
        }

    }
}
