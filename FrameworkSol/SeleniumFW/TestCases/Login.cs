﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Configuration;
using SeleniumFW.Utilities;


namespace SeleniumFW
{
    [TestFixture]
    public class Login : WebConenctor
    {
        public string actualResult;
        #region StartAll
        [SetUp]
        public void StartAll()
        {
            test = report.StartTest("LoginTest");
            OpenBrowser("Chrome");
        }
        #endregion

        #region LoginTest
        [Test]
        [TestCaseSource("GetData")]
        public void LoginTest(Dictionary<string, string> data)
        {
            NavigateTo("WebjectLoginPage");
            Type("USERNAME", data["USERNAME"]);
            Type("PASSWORD", data["PASSWORD"]);
            ClickOnElement("LOGINBUTTON");
            if (IsElementPresent("ADDCREDITCARDBUTTON"))
                actualResult = "Successful";
            else
                actualResult = "Unsuccessful";
            
            if (actualResult.Equals(data["EXPECTEDRESULT"]))
            {
                Console.WriteLine("Test is pass");
                ReportPass("Test is pass");
            }
            else
            {
                Console.WriteLine("Test is fail");
                ReportFail("Test is fail");
            }
        }

    public static object[] GetData()
    {
        ExcelReaderFile xls = new ExcelReaderFile(@"G:\Files\TestData.xlsx");
        //Read the data from excel file
        return DataUtil.getTestData(xls, "LoginTest");
    }
    #endregion

    #region CloseAll
    [TearDown]
    public void CloseAll()
    {
        CloseBrowser();
    }
    #endregion
}
}
